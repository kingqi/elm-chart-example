port module Main exposing (..)

import WebSocket
import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)
import Json.Decode exposing (..)
import Json.Decode.Extra exposing (..)


--结合okcoin数据完成k-line
{-
      buy(double): 买一价
      high(double): 最高价
      last(double): 最新成交价
      low(double): 最低价
      sell(double): 卖一价
      timestamp(long)：时间戳
      vol(double): 成交量(最近的24小时

   [{
       "channel":"ok_sub_spotcny_btc_kline_1min",
       "data":[
           ["1490337840000","995.37","996.75","995.36","996.75","9.112"],
           ["1490337840000","995.37","996.75","995.36","996.75","9.112"]
           [timestamp, open, highest, lowest, close, volume ]
       ]
   }]
-}


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Data =
    List Float


type alias Model =
    { lChartDatas : List Data
    , chartCreated : Bool
    }


type Msg
    = StartListen
    | StopListen
    | Recieve String


port createChart : String -> Cmd msg


port sendChartData : List Data -> Cmd msg


api : String
api =
    "wss://real.okcoin.cn:10440/websocket/okcoinapi"


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        StartListen ->
            ( model, WebSocket.send api "{'event':'addChannel','channel':'ok_sub_spotcny_btc_kline_1hour'}" )

        StopListen ->
            ( model, WebSocket.send api "{'event':'removeChannel','channel':'ok_sub_spotcny_btc_kline_1hour'}" )

        Recieve dataStr ->
            let
                dataListValue =
                    dataDecode dataStr
            in
                case dataListValue of
                    Just dataList ->
                        ( model, sendChartData dataList )

                    Nothing ->
                        ( model, Cmd.none )


view : Model -> Html Msg
view model =
    div
        []
        [ div
            [ id "chart"
            , style
                [ ( "width", "600px" )
                , ( "height", "400px" )
                ]
            ]
            []
        , div [] []
        , input [ Html.Attributes.type_ "button", Html.Attributes.value "Start Listen", onClick StartListen ]
            []
        , input [ Html.Attributes.type_ "button", Html.Attributes.value "Stop Listen", onClick StopListen, Html.Attributes.style [ ( "margin-left", "10px" ) ] ]
            []

        --, div [] [ text <| toString model.lChartDatas ]
        ]


subscriptions : Model -> Sub Msg
subscriptions model =
    WebSocket.listen api Recieve


init : ( Model, Cmd Msg )
init =
    ( Model [] False, createChart "chart" )


dataListDecoder : Decoder (List (List Data))
dataListDecoder =
    Json.Decode.list
        (field "data"
            (Json.Decode.list
                (Json.Decode.list parseFloat)
            )
        )


dataDecode : String -> Maybe (List Data)
dataDecode dataValue =
    let
        result =
            decodeString dataListDecoder dataValue
    in
        case result of
            Ok data ->
                List.head data

            Err msg ->
                Nothing
